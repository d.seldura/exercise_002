import React, { Component } from 'react';
import { Button, StyleSheet, View, Text } from 'react-native';

export default class ButtonBasics extends Component {
 
  state = { count: 0 }

  render() {
    return (
      <View style={styles.container}>
      <Text style={styles.textData}>{this.state.count}</Text>
        <View style={styles.buttonGreen}>
          <Button
            onPress={this._increment}
            title="Press Me"
            style={styles.buttonGreen}
            color="green"
          />
        </View>
        <View style={styles.buttonRed}>
          <Button
            onPress={this._reset}
            title="Reset"
            style={styles.buttonRed}
            color="red"
          />
        </View>
      </View>
    );
  }

  _increment = () => {
    this.setState({count:this.state.count +1 });
  }
  _reset = () => {
    this.setState({count:0 });
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  buttonGreen: {
    margin: 10,
    justifyContent: "space-between",
    backgroundColor: "#90A4AE",
    color: "#fff"
  },
  buttonRed: {
    margin: 10,
    justifyContent: "space-between",
    backgroundColor: "red",
    color: "#fff"
  },
  buttonSet:{
    margin: 20
  },
  textData: {
    fontSize: 150
  },
});
